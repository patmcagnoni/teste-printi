import { combineReducers } from 'redux';

import characters from './characters';
import authentication from './authentication';
import notification from './notification';

export default combineReducers({
    characters,
    authentication,
    notification
})