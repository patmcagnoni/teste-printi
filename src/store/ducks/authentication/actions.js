import * as t from './types';
import { getCharacters } from 'api/characters';
import { setCharacters, setTotal } from 'store/ducks/characters';
import { setLoading, setAlertError } from 'store/ducks/notification';

export const setAuthenticated = (isAuthenticated) => ({
  type: t.SET_AUTHENTICATED,
  payload: isAuthenticated,
})

export const authenticate = (privateKey, publicKey) => (dispatch) => {
  sessionStorage.setItem('keys', JSON.stringify({privateKey, publicKey}));
  dispatch(setLoading(true));

  getCharacters()
    .then(response => response.json())
    .then(parsedResponse => {
      if(parsedResponse.code !== 200){
        dispatch(setAlertError(true))
        dispatch(setLoading(false))
      }
      console.log(parsedResponse)
      const characters = parsedResponse.data.results;
      const total = parsedResponse.data.total
      dispatch(setCharacters(characters));
      dispatch(setTotal(total));
      dispatch(setAuthenticated({isAuthenticated: true}));
      dispatch(setLoading(false));
    })
    .catch(() => {
      dispatch(setAlertError(true))
      dispatch(setLoading(false))
    })   
}

