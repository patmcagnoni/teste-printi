import * as t from './types';

const INITIAL_STATE = {
    isAuthenticated: false,
}

export default function reducer(state = INITIAL_STATE, { type, payload }) {
    switch (type) {
    case t.SET_AUTHENTICATED:
        return { ...payload }
    default:
        return state
    }
}