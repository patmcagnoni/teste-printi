export const SET_CHARACTERS = 'characters/SET_CHARACTERS';
export const SET_CHARACTER = 'characters/SET_CHARACTER';
export const SET_LIMIT = 'characters/SET_LIMIT';
export const SET_PAGE= 'characters/SET_PAGE';
export const SET_ORDERBY = 'characters/SET_PRDERBY';
export const SET_TOTAL= 'characters/SET_TOTAL';
