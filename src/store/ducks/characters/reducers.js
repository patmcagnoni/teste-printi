import * as t from './types';

const INITIAL_STATE = {
    characters: [],
    page: 0,
    limit: 20,
    offset: 0,
    orderBy: 'name',
    total: 0,
    character: null
}

export default function reducer(state = INITIAL_STATE, { type, payload }) {
    switch (type) {
    case t.SET_CHARACTERS:
        return { ...state, characters: [...payload] }
    case t.SET_LIMIT:
        return { ...state, limit: payload }
    case t.SET_PAGE:
        return { ...state, page: payload }
    case t.SET_ORDERBY:
        return { ...state, orderBy: payload }
    case t.SET_TOTAL:
        return { ...state, total: payload }
    case t.SET_CHARACTER:
        return { ...state, character: payload}
    default:
        return state
    }
}