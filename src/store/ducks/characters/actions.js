import * as t from './types';
import { getCharacters, getCharacterById, getCharacterComics } from 'api/characters';
import { setLoading } from 'store/ducks/notification';

export const setCharacters = (characters) => ({
  type: t.SET_CHARACTERS,
  payload: characters,
})

export const setCharacter = (character) => ({
  type: t.SET_CHARACTER,
  payload: character,
})

const setPage = (page) => ({
  type: t.SET_PAGE,
  payload: page
})

export const setTotal = (total) => ({
  type: t.SET_TOTAL,
  payload: total
})

export const pageChange = (page) => (dispatch) => {
  dispatch(setPage(page));
  dispatch(fetchCharacters());
}

export const fetchCharacters = () => (dispatch, getState) => {
  const { limit, page, orderBy } = getState().characters;
  const offset = page * limit;
  const opts = {
    limit,
    offset,
    orderBy
  }
  dispatch(setLoading(true));
  getCharacters(opts)  
    .then(response => response.json())
    .then(parsedResponse => {
      dispatch(setCharacters(parsedResponse.data.results));
      dispatch(setTotal(parsedResponse.data.total));
      dispatch(setLoading(false));
    })
}

export const fetchCharacter = (id) => (dispatch) => {
  dispatch(setLoading(true));
  Promise.all([getCharacterById(id), getCharacterComics(id)])
    .then(responses => Promise.all(responses.map(res => res.json())))
    .then(([characterResponse, comicsResponse]) => {
      const character = {
        ...characterResponse.data.results[0],
        comics: [...comicsResponse.data.results]
      }
      dispatch(setCharacter(character));
      dispatch(setLoading(false));
    })
}



