import * as t from './types';

const INITIAL_STATE = {
    spinner: {
        isLoading: false,
    },
    alert: {
        error: {
            isOpen: false
        }
    }
}

export default function reducer(state = INITIAL_STATE, { type, payload }) {
    switch (type) {
    case t.SET_LOADING:
        return { ...state, spinner: { isLoading: payload } }
    case t.SET_ALERT_ERROR:
        return { ...state, alert: { error: { isOpen: payload } } }
    default:
        return state
    }
}