import * as t from './types';

export const setLoading = (isLoading) => ({
  type: t.SET_LOADING,
  payload: isLoading,
})

export const setAlertError = (isAlertError) => ({
  type: t.SET_ALERT_ERROR,
  payload: isAlertError,
})

