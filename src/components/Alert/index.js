import React from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';
import { Button } from '@material-ui/core';

const Alert = ({title, onConfirm, onCancel, type, message, isOpen}) => {
    return (
        <>
            {isOpen?
                <SweetAlert
                    title={title}
                    type={type}
                    onConfirm={onConfirm}
                    onCancel={onCancel}
                    btnSize="sm"
                    customButtons={<><Button onClick={onConfirm} variant="contained" color="primary" disableElevation>ok</Button></>}
                >
                    <p>{message}</p>
                </SweetAlert>
                :null
            }
        </>
    )
}

export default Alert;