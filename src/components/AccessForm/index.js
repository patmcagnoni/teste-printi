import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';

import { authenticate } from 'store/ducks/authentication';

import { Button, Box } from '@material-ui/core';
import Loader from 'components/Loader';

import * as S from './styled';

const AccessForm = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const isAuthenticated = useSelector(({authentication}) => authentication.isAuthenticated);
    const { isLoading } = useSelector(({notification}) => notification.spinner);
    
    const [privateKey, setPrivateKey] = useState('');
    const [publicKey, setPublicKey] = useState('');

    useEffect(() => {
        if(isAuthenticated){
            history.push("/characters")
        }
    }, [isAuthenticated])
    
    const handleSubmit = () => {
        dispatch(authenticate(privateKey, publicKey));
    }

    return (
    <S.Wrapper isLoading={isLoading}>
        <Loader isLoading={isLoading}/>
        <S.FormTitle>Dados de acesso</S.FormTitle>
        <Box display="flex" flexDirection="column">
            <S.FormInput 
                label="private_key" 
                variant="outlined" 
                value={privateKey}
                onChange={e => setPrivateKey(e.target.value)}
            />
            <S.FormInput 
                label="public_key" 
                variant="outlined"
                value={publicKey} 
                onChange={e => setPublicKey(e.target.value)}   
            />
        </Box>
        <Box textAlign="center">
            <Button 
                color="primary" 
                variant="contained" 
                size="medium"
                onClick={handleSubmit}
                >Acessar</Button>    
        </Box>
    </S.Wrapper>
)};

export default AccessForm;