import styled from 'styled-components';
import { TextField } from '@material-ui/core';

export const Wrapper = styled.form`
    opacity: ${({isLoading}) => isLoading? .5: 1};
    transition: opacity .2s;
    position: relative;
`;
export const FormTitle = styled.h2`
    text-align: center;
`;
export const FormInput = styled(TextField).attrs(() => ({
    type: "text"
}))`
    margin-bottom: 20px !important;
`;