import React from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { setAlertError } from 'store/ducks/notification';
import Alert from 'components/Alert';

import Home from 'pages/Home';
import Characters from 'pages/Characters';
import Character from 'pages/Character';

const App = () => {
    const dispatch = useDispatch(); 
    const { isOpen } = useSelector(({notification}) => notification.alert.error)
    
    const handleAlertConfirm = () => {
        dispatch(setAlertError(false));
    }

    return (
        <>
            <Alert 
                title="Ocorreu um erro" 
                onConfirm={handleAlertConfirm} 
                onCancel={handleAlertConfirm}
                message="Erro ao requisitar dados do servidor"
                type="error"
                isOpen={isOpen}
            />
            <Router>
                <Switch>
                    <Route exact path="/">
                        <Home />
                    </Route>
                    <Route exact path="/characters">
                        <Characters />
                    </Route>
                    <Route exact path="/characters/:id">
                        <Character />
                    </Route>
                </Switch>
            </Router>
        </>
    )
}

export default App;