import md5 from 'md5';

export const generateParams = (opts = {}) => {
    const { privateKey, publicKey } = JSON.parse(sessionStorage.getItem('keys'));
    const timestamp = Date.now();
    const apiKey = publicKey;
    const hash = md5(timestamp + privateKey + publicKey);
    const params = `?ts=${timestamp}` +
    `&apikey=${apiKey}` +
    `&hash=${hash}` +
    `${opts.limit? `&limit=${opts.limit}`: ''}` +
    `${opts.offset? `&offset=${opts.offset}`: ''}` +
    `${opts.orderBy? `&orderBy=${opts.orderBy}`: ''}` 

    return params;
}