import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Container } from '@material-ui/core';

import { fetchCharacter, setCharacter } from 'store/ducks/characters';

import Loader from 'components/Loader';

import * as S from './styled';

const Character = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const params = useParams();
    const id = params.id;
    const character = useSelector(({characters}) => characters.character);
    const { isLoading } = useSelector(({notification}) => notification.spinner);
    
    useEffect(() => {
        dispatch(fetchCharacter(id));
        return () => dispatch(setCharacter(null));
    },[])

    const handleButtonClick = () => {
        history.push('/characters');
    }
    
    return (
        <Container>
            <Loader isLoading={isLoading}/>
            {character?
                <>
                    <S.CharInfo>
                        <S.CharImg src={`${character.thumbnail.path}.${character.thumbnail.extension}`} />
                        <S.CharName>{character.name}</S.CharName>
                        <S.CharDesc>{character.description}</S.CharDesc>
                        <S.Button color="primary" variant="contained" onClick={handleButtonClick}>Voltar</S.Button>
                    </S.CharInfo>
                    <S.Comics>
                        <S.ComicsTitle>Fascículos</S.ComicsTitle>
                            <S.ComicsList>
                                {character.comics.map((comic) => (
                                    <S.Comic>
                                        <S.ComicImg src={comic.thumbnail.path + '.' + comic.thumbnail.extension}/>
                                        <S.ComicInfo>
                                            <S.ComicTitle><strong>Título: </strong>{comic.title}</S.ComicTitle>
                                            <S.ComicNumber><strong>número de capa: </strong>{comic.issueNumber}</S.ComicNumber>
                                        </S.ComicInfo>
                                        <S.ComicDesc>{comic.description}</S.ComicDesc>
                                    </S.Comic>
                                ))}
                            </S.ComicsList>
                    </S.Comics>   
                </>   
                :null
            }
        </Container>
    )
}

export default Character;