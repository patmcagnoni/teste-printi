import styled from 'styled-components';
import ButtonBase from '@material-ui/core/Button';

export const CharInfo = styled.div`
    display: grid;
    grid-template-columns: 15% 70% 15%;
    grid-template-rows: auto 1fr;
    row-gap: 10px;
    column-gap: 5px;
    margin-top: 60px;
    margin-bottom: 30px;

    @media only screen and (max-width: 960px) {
        grid-template-columns: 30% 70%;
        grid-template-rows: auto 3fr 1fr;
    }

    @media only screen and (max-width: 720px) {
        grid-template-columns: 1fr;
        grid-template-rows: repeat(4, auto);
    }
`;

export const CharImg = styled.img`
    width: 100%;
    grid-column: 1/2;
    grid-row: 1/3;
    align-self: center;

    @media only screen and (max-width: 720px) {
        grid-column: 1/2;
        grid-row: 2/3;
    }
`;

export const CharName = styled.div`
    border: 1px solid #333;
    grid-column: 2/3;
    grid-row: 1/2;
    padding: 5px;

    @media only screen and (max-width: 720px) {
        grid-column: 1/2;
        grid-row: 1/2;
    }
`;

export const CharDesc= styled.div`
    border: 1px solid #333;
    grid-column: 2/3;
    grid-row: 2/3;
    padding: 10px;

    @media only screen and (max-width: 720px) {
        grid-column: 1/2;
        grid-row: 3/4;
    }
`;

export const Button = styled(ButtonBase)`
    grid-column: 3/4;
    grid-row: 2/3;
    align-self: flex-end;
    height: 40px;
    max-width: 140px;

    @media only screen and (max-width: 960px) {
        grid-column: 2/3;
        grid-row: 3/4;
    }

    @media only screen and (max-width: 720px) {
        grid-column: 1/2;
        grid-row: 4/5;
        width: 100%;
        max-width: none;
    }
`;

export const Comics= styled.div``;

export const ComicsTitle= styled.h3`
    text-align: center;
    font-size: 18px;
    padding-bottom: 3px;
`;

export const ComicsList= styled.ul`
    padding: 0;
`;

export const Comic = styled.div`
    display: grid;
    grid-template-columns: 20% 80%;
    grid-template-rows: 30px 150px;
    border-top: 2px solid #ccc;
    padding: 10px;

    @media only screen and (max-width: 720px) {
        grid-template-columns: 1fr;
        grid-template-rows: auto 1fr auto;
        margin-bottom: 30px;
    }
`;

export const ComicImg = styled.img`
    grid-column: 1/2;
    grid-row: 1/3;
    max-width: 110px;
    width: 70%;
    align-self: center;
    justify-self: center;

    @media only screen and (max-width: 720px) {
        grid-column: 1/2;
        grid-row: 2/3;
        max-width: none;
        width: 100%;
        margin-bottom: 30px;
        margin-top: 30px;
    }
`;

export const ComicInfo = styled.div`
    display: flex;

    @media only screen and (max-width: 720px) {
        grid-column: 1/2;
        grid-row: 1/2;
    }
`;

export const ComicTitle = styled.div`
    grid-column: 2/3;
    grid-row: 1/2;
    width: 70%;
`;

export const ComicNumber = styled.div`
    grid-column: 2/3;
    grid-row: 1/2;
`;

export const ComicDesc = styled.div`
    grid-column: 2/3;
    grid-row: 2/3;

    @media only screen and (max-width: 720px) {
        grid-column: 1/2;
        grid-row: 3/4;
    }
`;
