import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import moment from 'moment';

import { pageChange } from 'store/ducks/characters';

import { Container } from '@material-ui/core';

import * as S from './styled';

const Characters = () => {
    const { characters, total, page } = useSelector(({characters}) => characters);
    const { isLoading } = useSelector(({notification}) => notification.spinner);
    const history = useHistory();
    const dispatch = useDispatch();

    const columns = [
        { field: 'name', headerName: 'Name', flex: 1},
        { field: 'description', headerName: 'Descrição', flex: 2, sortable: false,},
        { field: 'modified', headerName: 'Ultima atualização', flex: 1, valueFormatter: ({value}) => moment(value).format('DD/MM/YYYY') },
    ]

    const handleRowClick = (params) => {
        const id = params.row.id;
        
        history.push(`/characters/${id}`);
    }

    const handlePageChange = (params) => {
        dispatch(pageChange(params.page));
    }

    return (
        <Container>
            <S.PageTitle>Marvel Characters</S.PageTitle>
            <S.DataGridWrapper>
                <S.DataGrid 
                    rows={characters}
                    columns={columns} 
                    onRowClick={handleRowClick}
                    rowsPerPageOptions={[20]}
                    pageSize={20}
                    rowCount={total}
                    onPageChange={handlePageChange}
                    paginationMode={'server'}
                    loading={isLoading}
                    page={page}
                />
            </S.DataGridWrapper>
        </Container>
    )
};

export default Characters;