import styled from 'styled-components';
import { DataGrid as DataGridBase} from '@material-ui/data-grid';

export const PageTitle = styled.h1`
    text-align: center;
`;

export const DataGridWrapper = styled.div`
    height: 700px;
`;

export const DataGrid = styled(DataGridBase)`

    .MuiDataGrid-row {
        cursor: pointer;
    }
    
    .MuiDataGrid-cell {
        &:focus {
            outline:none !important;
        } 
    }
`;