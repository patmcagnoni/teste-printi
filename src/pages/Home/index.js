import React from 'react';
import AccessForm from 'components/AccessForm';
import { Container } from '@material-ui/core';

const Home = () => {
   
    return (
        <Container maxWidth="sm">    
            <AccessForm />
        </Container>
    )
}

export default Home;
