import { API_BASE_URL, API_CHAR_ENDPOINT } from './config';
import { generateParams } from 'util/params-generator';

export const getCharacters = (opts = {}) => {
    const params = generateParams(opts);
    const url = `${API_BASE_URL}/${API_CHAR_ENDPOINT}` + params;
    return fetch(url);
}

export const getCharacterById = (id) => {
    const params = generateParams();
    const url = `${API_BASE_URL}/${API_CHAR_ENDPOINT}/${id}` + params;
    return fetch(url);
}

export const getCharacterComics = (id, opts = {}) => {
    const params = generateParams(opts);
    const url = `${API_BASE_URL}/${API_CHAR_ENDPOINT}/${id}/comics` + params;
    return fetch(url)
}
