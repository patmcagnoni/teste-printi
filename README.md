# Marvel Heroes
![img1](https://i.imgur.com/i7mUOXt.png)
## About
This project fetches data from the Marvel API and displays it in a data table. By clicking on a row, a new page is rendered with info of the selected character.

This code is a job application requirement for a company called Printi. 
## Installation

### 1) Clone and install dependencies

* git clone git@bitbucket.org:patmcagnoni/teste-print.git
* `cd teste-print` - cd into the cloned project
* `npm install` or `yarn install` to install packages

### 2) Start your app

`npm run serve`

`yarn serve`

## Instructions

### 1) Insert your private and public keys
![img2](https://i.imgur.com/Lq8egCE.png)
### 2) Choose a character from the list
![img3](https://i.imgur.com/a2Dp2uM.png)
## Tech Stack

* React
* Redux
* Material UI

